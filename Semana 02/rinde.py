# Entrada
largo = float(input("Ingrese el largo del terreno: "))
ancho = float(input("Ingrese el ancho del terreno: "))

# Proceso
superficie = largo * ancho
rinde = superficie / 5

# Salida
print("El rinde del terreno es de", rinde, "quintales")
