import ventas
import pickle
import os


def menu():
    print("1 _ Cargar ventas")
    print("2 _ Generar archivo por región")
    print("3 _ Mostrar archivo")
    print("4 _ Buscar venta")
    print("5 _ Región con mas ventas")
    print("6 _ Salir")
    return int(input("Ingrese la opción: "))


def insertar_ordenado(v, venta):
    n = len(v)
    pos = n
    izq, der = 0, n-1
    while izq <= der:
        c = (izq + der) // 2
        if v[c].cod == venta.cod:
            pos = c
            break
        elif venta.cod > v[c].cod:
            izq = c + 1
        else:
            der = c - 1

    if izq > der:
        pos = izq

    v[pos:pos] = [venta]


def cargar_ventas(n):
    v = []
    for i in range(n):
        venta = ventas.cargar_venta_random()
        insertar_ordenado(v, venta)

    return v


def mostrar_ventas(v):
    for venta in v:
        print(ventas.write(venta))


def generar_archivo(v, region):
    archivo = open("ventas_region.dat", "wb")
    for i in range(len(v)):
        if v[i].region == region:
            pickle.dump(v[i], archivo)
    archivo.close()


def mostrar_archivo():
    archivo = open("ventas_region.dat", "rb")
    size = os.path.getsize("ventas_region.dat")
    while archivo.tell() < size:
        vta = pickle.load(archivo)
        print(ventas.write(vta))

    archivo.close()


def buscar_venta(v, cod):
    n = len(v)
    izq, der = 0, n-1
    while izq <= der:
        c = (izq + der) // 2
        if v[c].cod == cod:
            return v[c]
        elif cod > v[c].cod:
            izq = c + 1
        else:
            der = c - 1
    return None


def contar_ventas_region(v):
    cant = [0] * 24
    for vta in v:
        cant[vta.region] += 1

    return cant


def buscar_mayor(cantidad):
    mayor = cantidad[0]
    mayor_pos = 0
    for i in range(1, len(cantidad)):
        if cantidad[i] > mayor:
            mayor = cantidad[i]
            mayor_pos = i
    return mayor_pos


def principal():
    op = 1
    while op != 6:
        op = menu()
        if op == 1:
            n = int(input("Ingrese cantidad de ventas: "))
            v = cargar_ventas(n)
            mostrar_ventas(v)
        elif op == 2:
            region = int(input("Ingrese región: "))
            generar_archivo(v, region)
        elif op == 3:
            mostrar_archivo()
        elif op == 4:
            cod = int(input("Ingrese código a buscar: "))
            vta = buscar_venta(v, cod)
            if vta is None:
                print("No se encontró la venta. Ingrese los datos: ")
                vta = ventas.cargar_venta(cod)
                insertar_ordenado(v, vta)
            else:
                print("Se encontró la venta:")
                print(ventas.write(vta))
        elif op == 5:
            cantidad = contar_ventas_region(v)
            mayor = buscar_mayor(cantidad)
            print("La región con mas ventas es: ", mayor)


if __name__ == '__main__':
    principal()
