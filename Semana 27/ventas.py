import random


class Venta:
    def __init__(self, cod, fecha, region, monto):
        self.cod = cod
        self.fecha = fecha
        self.region = region
        self.monto = monto


def write(venta):
    return "Codigo:{} Fecha:{} Region:{} Monto:{}".format(
        venta.cod, venta.fecha, venta.region, venta.monto
    )


def generar_fecha_random():
    dia = random.randint(1, 28)
    mes = random.randint(1, 12)
    return "{}/{}/2019".format(dia, mes)


def cargar_venta_random():
    cod = random.randint(1, 10000)
    fecha = generar_fecha_random()
    region = random.randint(1, 23)
    monto = round(random.uniform(1000, 5000), 2)
    return Venta(cod, fecha, region, monto)


def cargar_venta(cod):
    fecha = input("Ingrese fecha: ")
    region = int(input("Ingrese región: "))
    monto = float(input("Ingrese monto: "))
    return Venta(cod, fecha, region, monto)


if __name__ == '__main__':
    for i in range(10):
        v = cargar_venta_random()
        print(write(v))
