import random

# Una empresa de colectivos desea generar estadística sobre sus
# pasajeros. Sus líneas están enumeradas de 0 a n, y las paradas de
# 0 a m.
#
# En primer lugar, se debe ingresar por teclado la cantidad de líneas
# de colectivo y el número de paradas a procesar.
#
# Luego cargar una tabla de doble entrada, donde cada fila represente
# a una de las líneas y cada columna una parada, indicando la cantidad
# de pasajeros que subieron al colectivo.
#
# Con esa información establecer:
#
#     Cuál fue el total de pasajeros transportado por cada una de las
#     líneas
#     Cuál fue la cantidad promedio de pasajeros, para una parada que
#     se ingresa por teclado
#     Cuál fue la menor cantidad de pasajeros, para una línea que se
#     ingresa por teclado.
#     Cuál fue la recaudación de la empresa en el período analizado,
#     sabiendo que el precio del boleto es de $31.9.
MENSAJE = "Ingrese la cantidad de pasajeros de la linea {} parada {}:"


def cargar_pasajeros(lineas, paradas):
    m = [[0] * paradas for i in range(lineas)]
    for l in range(lineas):
        for p in range(paradas):
            # m[l][p] = int(input(MENSAJE.format(l, p)))
            m[l][p] = random.randint(0, 20)
    return m


def total_por_linea(m):
    lineas = len(m)
    paradas = len(m[0])
    totales = [0] * lineas
    for i in range(lineas):
        for j in range(paradas):
            totales[i] += m[i][j]
    return totales


def promedio_parada(m, parada):
    lineas = len(m)
    suma = 0
    for i in range(lineas):
        suma += m[i][parada]
    return suma / lineas


def menor_linea(m, linea):
    paradas = len(m[0])
    menor = m[linea][0]
    for j in range(1, paradas):
        if m[linea][j] < menor:
            menor = m[linea][j]
    return menor


def totalizar(m):
    lineas = len(m)
    paradas = len(m[0])
    total = 0
    for i in range(lineas):
        for j in range(paradas):
            total += m[i][j]
    return total


def principal():
    lineas = int(input("Ingrese la cantidad de líneas: "))
    paradas = int(input("Ingrese la cantidad de paradas: "))

    pasajeros = cargar_pasajeros(lineas, paradas)
    print(pasajeros)
    totales_linea = total_por_linea(pasajeros)
    for i in range(lineas):
        print("Pasajeros de la linea", i, ":", totales_linea[i])

    parada = int(input("Ingrese número de parada: "))
    promedio = promedio_parada(pasajeros, parada)
    print("Promedio de pasajeros de la parada: ", promedio)

    linea = int(input("Ingrese número de linea: "))
    menor = menor_linea(pasajeros, linea)
    print("La menor cantidad de pasajeros fue:", menor)

    total_pasajeros = totalizar(pasajeros)
    recaudacion = total_pasajeros * 31.9
    print("La recaudación total fue:", recaudacion)


if __name__ == '__main__':
    principal()
