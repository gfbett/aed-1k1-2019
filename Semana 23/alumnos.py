NOMBRE_ARCHIVO = "alumnos.csv"


class Alumno:
    def __init__(self, legajo, nombre, nota):
        self.legajo = legajo
        self.nombre = nombre
        self.nota = nota


def write(alumno):
    return "Legajo:{} nombre:{} nota:{}".format(
        alumno.legajo,
        alumno.nombre,
        alumno.nota
    )


def mostrar_alumnos(v):
    for alumno in v:
        print(write(alumno))


def cargar_alumno():
    legajo = int(input("Ingrese legajo: "))
    nombre = input("Ingrese nombre: ")
    nota = int(input("Ingrese nota: "))
    return Alumno(legajo, nombre, nota)


def grabar_alumnos(v):
    archivo = open(NOMBRE_ARCHIVO, "wt")
    template = "{}, {}, {}\n"

    for a in v:
        linea = template.format(a.legajo, a.nombre,  a.nota)
        archivo.write(linea)
    archivo.close()


def cargar_archivo():
    v = []

    archivo = open(NOMBRE_ARCHIVO, "rt")
    for linea in archivo:
        partes = linea.split(",")
        alumno = Alumno(partes[0].strip(),
                        partes[1].strip(),
                        partes[2].strip())
        v.append(alumno)

    archivo.close()
    return v


def generar_html(v):
    cabecera = "<html><head/><body><table><tr>" \
               "<td>Legajo</td>" \
               "<td>Nombre</td>" \
               "<td>Nota</td></tr>"
    final = "</table></body></html>"
    archivo = open("alumnos.html", "wt")
    archivo.write(cabecera)
    for alumno in v:
        archivo.write("<tr>")
        archivo.write("<td>" + str(alumno.legajo) + "</td>")
        archivo.write("<td>" + str(alumno.nombre) + "</td>")
        archivo.write("<td>" + str(alumno.nota) + "</td>")
        archivo.write("</tr>")
    archivo.write(final)
    archivo.close()

def principal():
    op = 1
    v = cargar_archivo()
    while op != 4:
        print("1_ Cargar alumno")
        print("2_ Listar alumno")
        print("3_ Generar HTML")
        print("4_ Salir")
        op = int(input("Ingrese opción: "))

        if op == 1:
            alumno = cargar_alumno()
            v.append(alumno)
        elif op == 2:
            mostrar_alumnos(v)
        elif op == 3:
            generar_html(v)
        elif op == 4:
            grabar_alumnos(v)


if __name__ == '__main__':
    principal()
