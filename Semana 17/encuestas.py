import random

def cargar(n):
    numeros = [0] * n
    colores = [0] * n
    edades = [0] * n
    materiales = [0] * n
    for i in range(n):
        numeros[i] = random.randint(30, 50)
        colores[i] = random.choice(("blanco", "negro", "azul"))
        edades[i] = random.randint(1, 100)
        materiales[i] = random.randint(0, 1)
    return numeros, colores, edades, materiales


def calcular_nro_promedio(numeros):
    acum = 0
    for i in range(len(numeros)):
        acum += numeros[i]
    return acum // len(numeros)


def calcular_color_preferido(edades, colores):
    blanco = negro = azul = 0
    n = len(edades)
    for i in range(n):
        edad = edades[i]
        if 10 <= edad <= 18:
            color = colores[i]
            if color == "blanco":
                blanco += 1
            elif color == "negro":
                negro += 1
            else:
                azul += 1

    if blanco == negro == azul == 0:
        return "No hay encuentas para las edades"
    elif blanco > negro and blanco > azul:
        return "blanco"
    elif negro > azul:
        return "negro"
    else:
        return "azul"


def calcular_material_preferido(edades, materiales):
    contador = [0] * 2
    n = len(edades)
    for i in range(n):
        edad = edades[i]
        if 19 <= edad <= 25:
            material = materiales[i]
            contador[material] += 1

    if contador[0] == contador[1] == 0:
        return "No hay encuentas para las edades"
    elif contador[0] > contador[1]:
        return "cuero"
    else:
        return "tela"


def calcular_demanda(numeros):
    cantidades = [0] * 6
    for numero in numeros:
        if 35 <= numero <= 40:
            cantidades[numero - 35] += 1
    return cantidades


def buscar_mayor(v):
    mayor = 0
    mayor_pos = 0
    for i in range(len(v)):
        if i == 0 or v[i] > mayor:
            mayor = v[i]
            mayor_pos = i
            
    return mayor_pos
    

def buscar_menor(v):
    menor = 0
    menor_pos = 0
    for i in range(len(v)):
        if i == 0 or v[i] < menor:
            menor = v[i]
            menor_pos = i
            
    return menor_pos


def principal():
    n = int(input("Ingrese la cantidad de encuestas: "))
    while n <= 0:
        n = int(input("Error debe ser mayor a cero. Ingrese la cantidad de encuestas: "))

    numeros, colores, edades, materiales = cargar(n)
    print(numeros)
    print(colores)
    print(edades)
    print(materiales)

    nro_promedio = calcular_nro_promedio(numeros)
    print("El número promedio es:", nro_promedio)

    color_preferido = calcular_color_preferido(edades, colores)
    print("El color preferido es:", color_preferido)

    material_preferido = calcular_material_preferido(edades, materiales)
    print("El material preferido es:", material_preferido)

    demanda_numeros = calcular_demanda(numeros)
    print(demanda_numeros)
    mayor = buscar_mayor(demanda_numeros)
    menor = buscar_menor(demanda_numeros)
    print("El número con mayor demanda es:", mayor + 35)
    print("El número con menor demanda es:", menor + 35)

if __name__ == '__main__':
    principal()
