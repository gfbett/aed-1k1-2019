# Carga de datos

codigo1 = input("Ingrese el código del primer curso: ")
mujeres1 = int(input("Ingrese la cantidad de mujeres: "))
varones1 = int(input("Ingrese la cantidad de varones: "))

codigo2 = input("Ingrese el código del segundo curso: ")
mujeres2 = int(input("Ingrese la cantidad de mujeres: "))
varones2 = int(input("Ingrese la cantidad de varones: "))

codigo3 = input("Ingrese el código del tercer curso: ")
mujeres3 = int(input("Ingrese la cantidad de mujeres: "))
varones3 = int(input("Ingrese la cantidad de varones: "))

cupo_maximo = int(input("Ingrese el cupo máximo de los cursos: "))

# Procesos

total_curso1 = mujeres1 + varones1
total_curso2 = mujeres2 + varones2
total_curso3 = mujeres3 + varones3

# Codigo del curso con menos alumnos
if total_curso1 < total_curso2 and total_curso1 < total_curso3:
    codigo_menor = codigo1
elif total_curso2 < total_curso3:
    codigo_menor = codigo2
else:
    codigo_menor = codigo3

# Porcentaje de mujeres
porc_mujeres1 = mujeres1 * 100 / total_curso1
porc_mujeres2 = mujeres2 * 100 / total_curso2
porc_mujeres3 = mujeres3 * 100 / total_curso3

# Porcentajes de varones
porc_varones1 = varones1 * 100 / total_curso1
porc_varones2 = varones2 * 100 / total_curso2
porc_varones3 = varones3 * 100 / total_curso3

promedio = (total_curso1 + total_curso2 + total_curso3) / 3

print("El código del curso con menor cantidad de alumnos es:", codigo_menor)
print("El curso", codigo1, "tiene", porc_mujeres1, "% mujeres y",
      porc_varones1, "% varones")
print("El curso", codigo2, "tiene", porc_mujeres2, "% mujeres y",
      porc_varones2, "% varones")
print("El curso", codigo3, "tiene", porc_mujeres3, "% mujeres y",
      porc_varones3, "% varones")

print("El promedio de alumnos por curso es:", promedio)

if total_curso1 > cupo_maximo \
        or total_curso2 > cupo_maximo \
        or total_curso3 > cupo_maximo:
    print("Se debe habilitar una nueva división!")
