def cargar_precipitaciones():
    v = [0] * 12
    for i in range(12):
        v[i] = int(input("Ingrese la precipitación del mes " +
                         str(i + 1) + ": "))
    return v


def menu():
    print("1 _ Promedio anual")
    print("2 _ Promedio por trimestre ")
    print("3 _ Mes mas seco")
    print("4 _ Salir")
    return int(input("Ingrese opción: "))


def calcular_promedio(precipitaciones):
    suma = 0
    for valor in precipitaciones:
        suma += valor
    promedio = suma / 12
    return promedio


def mes_mas_seco(precipitaciones):
    menor = 0
    menor_mes = 0
    for i in range(12):
        if i == 0 or precipitaciones[i] < menor:
            menor = precipitaciones[i]
            menor_mes = i
    return menor_mes


def promedio_trimestre(precipitaciones, trimestre):
    desde = 3 * (trimestre - 1)
    hasta = desde + 3
    suma = 0
    for i in range(desde, hasta):
        suma += precipitaciones[i]
    return suma / 3


def main():
    precipitaciones = cargar_precipitaciones()
    print(precipitaciones)
    op = 1
    while op != 4:
        op = menu()
        if op == 1:
            promedio = calcular_promedio(precipitaciones)
            print("El promedio anual es", promedio)
        elif op == 2:
            trimestre = int(input("Indique trimestre: "))
            promedio = promedio_trimestre(precipitaciones, trimestre)
            print("El promedio del trimestre es:", promedio)
        elif op == 3:
            mes = mes_mas_seco(precipitaciones)
            print("El mes con menor precipitaciones fue: ", (mes + 1))
        elif op == 4:
            pass


if __name__ == '__main__':
    main()
