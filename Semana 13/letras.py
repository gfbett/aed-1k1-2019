def es_letra(car):
    return car not in (" ", ",", ".")


texto = "Hola mundo ola"

# inicializacion
clet = cpal = palabras_ultima = 0
ultima_letra_anterior = ""

for car in texto:
    if es_letra(car):
        # dentro palabra
        clet += 1
        if clet == 1 and ultima_letra_anterior == car:
            palabras_ultima += 1

        ultima_letra = car
    else:
        if clet > 0:
            # fin palabra
            cpal += 0
            ultima_letra_anterior = ultima_letra
        clet = 0

# fin texto

