import random

class Cabania:
    def __init__(self, nombre, dni, monto, tipo):
        self.nombre = nombre
        self.dni = dni
        self.monto = monto
        self.tipo = tipo


def menu():
    print("1 _ Cargar alquileres")
    print("2 _ Cantidad mayor a x")
    print("3 _ Montos por tipo")
    print("4 _ Mostrar por dni")
    print("5 _ Alquileres de menor monto")
    print("6 _ Salir")
    return int(input("Ingrese opción: "))


def cargar_alquiler():
    nombre = input("Ingrese nombre del cliente: ")
    dni = int(input("Ingrese el dni: "))
    monto = float(input("Ingrese el monto del alquiler: "))
    tipo = int(input("Ingrese tipo: "))
    while tipo < 0 or tipo > 9:
        tipo = int(input("Error. Ingrese tipo: "))
    return Cabania(nombre, dni, monto, tipo)


def cargar_alquiler_random():
    nombre = chr(random.randint(65, 90))
    dni = random.randint(1000000, 60000000)
    monto = random.randint(5, 20) * 1000 / 10
    tipo = random.randint(0, 9)
    return Cabania(nombre, dni, monto, tipo)


def cargar_datos():
    n = int(input("Ingrese cantidad de alquileres: "))
    v = [None] * n
    for i in range(n):
        v[i] = cargar_alquiler_random()
    return v


def write(alquiler):
    print("Alquiler: ",
          "Nombre:", alquiler.nombre,
          "Dni:", alquiler.dni,
          "Monto:", alquiler.monto,
          "Tipo:", alquiler.tipo)


def mostrar_alquileres(v):
    for alquiler in v:
        write(alquiler)


def contar_mayores(v, x):
    cantidad = 0
    for alquiler in v:
        if alquiler.monto > x:
            cantidad += 1
    return cantidad


def totales_por_tipo(v):
    totales = [0] * 10
    for i in range(len(v)):
        tipo = v[i].tipo
        monto = v[i].monto
        totales[tipo] += monto
    return totales


def ordenar(v):
    n = len(v)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if v[i].dni < v[j].dni:
                v[i], v[j] = v[j], v[i]


def menores_monto(v):
    m = []
    menor = v[0]
    m.append(menor)
    for i in range(1, len(v)):
        if v[i].monto < menor.monto:
            menor = v[i]
            m = []
            m.append(v[i])
        elif v[i].monto == menor.monto:
            m.append(v[i])
    return m



def principal():
    op = 0
    v = []
    while op != 6:
        op = menu()
        if op == 1:
            v = cargar_datos()
            mostrar_alquileres(v)
        elif op == 2:
            x = float(input("Ingrese monto a comparar: "))
            mayores = contar_mayores(v, x)
            print("Hay", mayores, "alquileres con monto mayor a $", x)
        elif op == 3:
            totales = totales_por_tipo(v)
            for i in range(len(totales)):
                print("El total del tipo", i, "es $", totales[i])
        elif op == 4:
            ordenar(v)
            mostrar_alquileres(v)
        elif op == 5:
            menores = menores_monto(v)
            print("Alquileres con menor monto:")
            mostrar_alquileres(menores)


if __name__ == '__main__':
    principal()
