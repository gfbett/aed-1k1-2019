import piezas
import pickle
import os


def menu():
    print("1 _ Cargar piezas")
    print("2 _ Listar piezas por precio")
    print("3 _ Generar archivo")
    print("4 _ Mostrar archivo")
    print("5 _ Salir")
    return int(input("Ingrese la opción: "))


def cargar_piezas(n):
    v = [None] * n
    for i in range(n):
        v[i] = piezas.cargar_pieza_random()
    return v


def cargar_tipos():
    v = []
    archivo = open("productos.txt", "rt")
    for linea in archivo:
        v.append(linea.strip())
    archivo.close()
    return v


def menu():
    print("1 _ Cargar piezas")
    print("2 _ Listar piezas por precio")
    print("3 _ Generar archivo")
    print("4 _ Mostrar archivo")
    print("5 _ Salir")
    return int(input("Ingrese la opción: "))


def ordenar(v):
    n = len(v)
    for i in range(n - 1):
        for j in range(i+1, n):
            if v[i].precio > v[j].precio:
                v[i], v[j] = v[j], v[i]


def mostrar_piezas(v, descripciones):
    for pieza in v:
        piezas.write_descripcion(pieza, descripciones)


def generar_archivo(v, tipo):
    archivo = open("piezas.bin", "wb")
    for pieza in v:
        if pieza.tipo == tipo:
            pickle.dump(pieza, archivo)
    archivo.close()


def mostrar_archivo():
    archivo = open("piezas.bin", "rb")
    size = os.path.getsize("piezas.bin")
    while archivo.tell() < size:
        pieza = pickle.load(archivo)
        piezas.write(pieza)

    archivo.close()


def principal():
    op = 1
    v = []
    descripciones = cargar_tipos()

    while op != 5:
        op = menu()
        if op == 1:
            n = int(input("Ingrese cantidad de piezas:"))
            v = cargar_piezas(n)
        elif op == 2:
            ordenar(v)
            mostrar_piezas(v, descripciones)
        elif op == 3:
            tipo = int(input("Ingrese tipo a generar: "))
            generar_archivo(v, tipo)
        elif op == 4:
            mostrar_archivo()


if __name__ == '__main__':
    principal()
