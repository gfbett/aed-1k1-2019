import random


class Pieza:
    def __init__(self, descripcion, precio, tipo):
        self.descripcion = descripcion
        self.precio = precio
        self.tipo = tipo


def write(pieza):
    print("Descripcion:", pieza.descripcion,
          "- Precio:", pieza.precio,
          "- Tipo:", pieza.tipo)


def write_descripcion(pieza, descripciones):
    print("Descripcion:", pieza.descripcion,
          "- Precio:", pieza.precio,
          "- Tipo:", descripciones[pieza.tipo])


def cargar_pieza():
    descripcion = input("Ingrese descripcion: ")
    precio = float(input("Ingrese precio: "))
    tipo = int(input("Ingrese tipo: "))
    return Pieza(descripcion, precio, tipo)


def cargar_pieza_random():
    descripcion = chr(random.randint(65, 90))
    precio = random.randint(10000, 20000) / 100
    tipo = random.randint(0, 9)
    return Pieza(descripcion, precio, tipo)


if __name__ == '__main__':
    pieza = cargar_pieza_random()
    write(pieza)

