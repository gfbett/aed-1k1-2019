# Entrada
nota_p1 = int(input("Ingrese la nota del primer parcial: "))
nota_p2 = int(input("Ingrese la nota del segundo parcial: "))
nota_tp = int(input("Ingrese la nota del trabajo práctico: "))

# Proceso
promedio = (nota_p1 + nota_p2 + nota_tp) / 3

if promedio < 4:
    condicion = "Libre"
else:
    if promedio < 8:
        condicion = "Regular"
    else:
        condicion = "Promocionado"

# Salida
print("La condición del alumno es:", condicion)
