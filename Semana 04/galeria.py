cuadro1 = int(input("Ingrese el año del primer cuadro: "))
cuadro2 = int(input("Ingrese el año del segundo cuadro: "))
cuadro3 = int(input("Ingrese el año del tercer cuadro: "))

# Verificación anteriores siglo XX
if cuadro1 < 1901 and cuadro2 < 1901 and cuadro3 < 1901:
    print("Todos los cuadros son anteriores al siglo XX")

# Cuadros de menos de 10 años
if cuadro1 > 2009:
    nuevo1 = 1
else:
    nuevo1 = 0

if cuadro2 > 2009:
    nuevo2 = 1
else:
    nuevo2 = 0

if cuadro3 > 2009:
    nuevo3 = 1
else:
    nuevo3 = 0

nuevos = nuevo1 + nuevo2 + nuevo3
print("Hay", nuevos, "cuadros con menos de 10 años")

# Renovar stock
if nuevos == 0:
    print("Hay que renovar el stock")
