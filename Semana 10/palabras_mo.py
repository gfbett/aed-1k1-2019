def es_letra(car):
    res = False
    if car != ' ' and car != '.':
        res =  True
    return res

def es_xy(car):
    return car == 'x' or car == 'y'

#texto = input("Ingrese el texto: ")
texto = 'hola mundo como    va xilofon caja monoxido momoxy trombon.'


# 1 - Inicialización
c_letras = c_palabras = total_letras = 0
palabras_mas_cuatro = palabras_xy = 0
tiene_xy = False
bandera_m = False
cantidad_mo = palabras_mo =0


for caracter in texto:
    if es_letra(caracter):
        # 2 - Dentro de una palabra
        c_letras += 1
        if es_xy(caracter):
            tiene_xy = True
        if caracter == 'm':
            bandera_m = True
        else:
            if caracter == 'o' and bandera_m:
                cantidad_mo += 1
            bandera_m = False

    else:
        # 3 - Fin de palabra
        if c_letras > 0:
            total_letras += c_letras
            c_palabras += 1
            if c_letras > 4:
                palabras_mas_cuatro += 1
            if tiene_xy:
                palabras_xy += 1
            if cantidad_mo == 1:
                palabras_mo += 1


        c_letras = 0
        tiene_xy = False
        bandera_m = False
        cantidad_mo = 0


    print(caracter, bandera_m, cantidad_mo, palabras_mo)

# 4 - Fin de texto
promedio = total_letras / c_palabras

print(texto)
print("La cantidad de palabras con mas de 4 letras es:", palabras_mas_cuatro)
print("La cantidad de palabras con x o y es:", palabras_xy)
print("El promedio de letras por palabras es:", promedio)
print("La cantidad de palabras con mo una vez es:", palabras_mo)
