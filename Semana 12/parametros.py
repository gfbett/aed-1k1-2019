def mostrar_persona(nombre, apellido, edad = 18, sexo="Femenino"):
    print("\nNombre", nombre)
    print("Apellido", apellido)
    print("Edad", edad)
    print("Sexo", sexo)


def sumar(a, b, c=0, d=0):
    return a + b + c + d


def sumar_v2(*args):
    suma = 0
    for x in args:
        suma += x
    return suma


mostrar_persona("Juan", "Perez", sexo= "Masculino")
mostrar_persona(apellido="Juan", nombre="Perez", edad=30)
print(sumar(1, 2, 3), sumar(1, 2, 3, 4), sumar(1, 2), sep="\t", end="!!!\n")

print(sumar_v2())
print(sumar_v2(1))
print(sumar_v2(1, 2))
print(sumar_v2(1, 2, 3))
print(sumar_v2(1, 2, 3, 4))
print(sumar_v2(1, 2, 3, 4, 5))
print(sumar_v2(1, 2, 3, 4, 5, 6))
print(sumar_v2(1, 2, 3, 4, 5, 6, 7))
print(sumar_v2(1, 2, 3, 4, 5, 6, 7, 8))
print(sumar_v2(1, 2, 3, 4, 5, 6, 7, 8, 9))
print(sumar_v2(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
