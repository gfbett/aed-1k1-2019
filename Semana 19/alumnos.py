import random
# Cargar un arreglo de n alumnos. De cada alumno se
# conoce:
# - legajo
# - nombre
# - nota
#
# Determinar los datos del alumno con mayor nota.


class Alumno:
    def __init__(self, legajo, nombre, nota):
        self.legajo = legajo
        self.nombre = nombre
        self.nota = nota


def write(alumno):
    print("Alumno: legajo:", alumno.legajo, "Nombre:", alumno.nombre,
          "Nota:", alumno.nota)


def mostrar_alumnos(v):
    for alumno in v:
        write(alumno)


def buscar_mayor(v):
    mayor = 0
    mayor_alumno = None
    for i in range(len(v)):
        if i == 0 or v[i].nota > mayor:
            mayor = v[i].nota
            mayor_alumno = v[i]
    return mayor_alumno


def principal():
    n = int(input("Ingrese cantidad de alumnos: "))
    v = cargar_vector_alumnos(n)

    mostrar_alumnos(v)
    mayor = buscar_mayor(v)
    print("El alumno con mayor nota es: ")
    write(mayor)


def cargar_vector_alumnos(n):
    v = [None] * n
    for i in range(n):
        v[i] = cargar_alumno_random()
    return v


def cargar_alumno():
    legajo = int(input("Ingrese el legajo: "))
    nombre = input("Ingrese el nombre: ")
    nota = int(input("Ingrese la nota: "))
    return Alumno(legajo, nombre, nota)


def cargar_alumno_random():
    legajo = random.randint(50000, 90000)
    nombre = ""
    for i in range(5):
        nombre += chr(random.randint(65, 90))
    nota = random.randint(1, 10)
    return Alumno(legajo, nombre, nota)


if __name__ == '__main__':
    principal()
