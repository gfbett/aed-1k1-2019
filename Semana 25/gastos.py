import random


class Gasto:
    def __init__(self, codigo, descripcion, mes, sucursal, importe):
        self.codigo = codigo
        self.descripcion = descripcion
        self.mes = mes
        self.sucursal = sucursal
        self.importe = importe


def write(gasto):
    print("Codigo:", gasto.codigo,
          "Descripcion:", gasto.descripcion,
          "Mes:", gasto.mes,
          "Sucursal:", gasto.sucursal,
          "Importe:", gasto.importe
          )


def cargar_gasto_random():
    codigo = random.randint(1, 10000)
    descripcion = chr(random.randint(65, 90))
    mes = random.randint(1, 12)
    sucursal = random.randint(0, 2)
    importe = round(random.uniform(100, 2000), 2)
    gasto = Gasto(codigo, descripcion, mes, sucursal, importe)
    return gasto


if __name__ == '__main__':
    g = cargar_gasto_random()
    write(g)
