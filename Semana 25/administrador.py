import pickle
import os
import gastos

def menu():
    print("1 _ Cargar gastos")
    print("2 _ Mostrar gastos")
    print("3 _ Generar archivo")
    print("4 _ Mostrar archivo")
    print("5 _ Generar matriz")
    print("6 _ Total por mes")
    print("7 _ Salir")
    return int(input("Ingrese la opción: "))


def insertar_ordenado(v, gasto):
    n = len(v)
    izq = 0
    der = n - 1
    while izq <= der:
        c = (izq + der) // 2
        if v[c].codigo == gasto.codigo:
            pos = c
            break
        if gasto.codigo > v[c].codigo:
            izq = c + 1
        else:
            der = c - 1
    if izq > der:
        pos = izq

    v[pos:pos] = [gasto]



def cargar_gastos(n):
    v = []
    for i in range(n):
        gasto = gastos.cargar_gasto_random()
        insertar_ordenado(v, gasto)
    return v


def mostrar_gastos(v):
    for gasto in v:
        gastos.write(gasto)


def generar_archivo(v, valor):
    archivo = open("gastos.bin", "wb")
    for gasto in v:
        if gasto.importe > valor:
            pickle.dump(gasto, archivo)
    archivo.close()


def mostrar_archivo():
    archivo = open("gastos.bin", "rb")
    size = os.path.getsize("gastos.bin")

    while archivo.tell() < size:
        gasto = pickle.load(archivo)
        gastos.write(gasto)

    archivo.close()


def generar_matriz():
    m = [[0] * 3 for i in range(12)]
    archivo = open("gastos.bin", "rb")
    size = os.path.getsize("gastos.bin")

    while archivo.tell() < size:
        gasto = pickle.load(archivo)
        m[gasto.mes - 1 ][gasto.sucursal] += gasto.importe

    archivo.close()

    return m


def mostrar_matriz(matriz):
    for i in range(len(matriz)):
        print("{:2}".format(i), end="|")
        for j in range(len(matriz[i])):
            print("{:10}".format(round(matriz[i][j], 2)), end="|")
        print()


def total_mes(matriz, mes):
    suma = 0
    for i in range(3):
        suma += matriz[mes - 1][i]
    return suma


def principal():
    op = 1
    v = []
    while op != 7:
        op = menu()
        if op == 1:
            n = int(input("Ingrese cantidad de gastos: "))
            v = cargar_gastos(n)
        elif op == 2:
            mostrar_gastos(v)
        elif op == 3:
            valor = float(input("Ingrese valor para generar el archivo:"))
            generar_archivo(v, valor)
        elif op == 4:
            mostrar_archivo()
        elif op == 5:
            matriz = generar_matriz()
            mostrar_matriz(matriz)
        elif op == 6:
            mes = int(input("Ingrese mes a totalizar:"))
            total = total_mes(matriz, mes)
            print("El total del mes es:", total)


if __name__ == '__main__':
    principal()
