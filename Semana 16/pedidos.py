import random


def cargar_datos(n):
    numeros = [0] * n
    montos = [0] * n
    semanas = [0] * n

    for i in range(n):
        numeros[i] = random.randint(0, 29)
        montos[i] = random.randint(1, 10000) / 100
        semanas[i] = random.randint(0, 52)

    return numeros, montos, semanas


def calcular_monto_total(montos):

    total = 0
    for monto in montos:
        total += monto

    return total


def contar_por_semana(semanas):
    cant = [0] * 53
    for x in semanas:
        cant[x] += 1
    return cant


def buscar_mayor(v):
    mayor = v[0]
    mayor_pos = 0
    for i in range(1, len(v)):
        if v[i] > mayor:
            mayor = v[i]
            mayor_pos = i
    return mayor_pos


def calcular_recaudacion(numeros, montos):
    cont = [0] * 30

    for i in range(len(numeros)):
        cliente = numeros[i]
        monto = montos[i]
        cont[cliente] += monto

    return cont


def listar_pedidos(numeros, montos, semanas):
    print("--------Listado de pedidos------")
    for i in range(len(numeros)):
        print("Cliente:", numeros[i], "Monto:", montos[i],
              "Semana:", semanas[i])
    print("--------------------------------")


def ordenar(numeros, montos, semanas):
    n = len(numeros)
    for i in range(n - 1):
        for j in range(i + 1, n):
            if montos[i] < montos[j]:
                montos[i], montos[j] = montos[j], montos[i]
                semanas[i], semanas[j] = semanas[j], semanas[i]
                numeros[i], numeros[j] = numeros[j], numeros[i]


def main():
    n = int(input("Ingrese cantidad de clientes: "))
    numeros, montos, semanas = cargar_datos(n)

    listar_pedidos(numeros, montos, semanas)
    ordenar(numeros, montos, semanas)
    listar_pedidos(numeros, montos, semanas)

    monto_total = calcular_monto_total(montos)
    print("El monto total es:", monto_total)

    cantidad_semana = contar_por_semana(semanas)
    mayor_semana = buscar_mayor(cantidad_semana)
    print("La semana con mas pedidos es:", mayor_semana)

    recaudacion = calcular_recaudacion(numeros, montos)
    print("Monto recaudado por cliente: ")
    for i in range(len(recaudacion)):
        print("Cliente:", i, "Recaudado:", recaudacion[i])

if __name__ == '__main__':
    main()
