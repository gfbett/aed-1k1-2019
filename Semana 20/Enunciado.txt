Una empresa dedicada a la producción de alimentos balanceados
quiere llevar las estadisticas de sus ventas, para ello solicita
un programa que:

- Cargue una lista con los datos de n ventas realizadas en el semestre.
    De cada venta se conoce:
        - Id de factura
        - Nombre del cliente
        - Semana (1, 26)
        - Familia de producto (0-9)
        - Monto
- Muestre todos las facturas ingresadas
- Genere un listado de todas las ventas de una familia de producto
    ingresada por teclado, el listado debe estar ordenado por número
    de semana
- Determine los datos de la venta con mayor importe
- Permita buscar si se cargó una factura por número, y si se encuentra
    mostrar sus datos

