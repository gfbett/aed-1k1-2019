import random

class Venta:
    def __init__(self, nro, nombre, semana, familia, monto):
        self.nro = nro
        self.nombre = nombre
        self.semana = semana
        self.familia = familia
        self.monto = monto


def write(vta):
    print("Venta nro:", vta.nro, "nombre:", vta.nombre, "semana:",
        vta.semana, "familia:", vta.familia, "monto:", vta.monto)


def menu():
    print("1 _ Cargar Datos")
    print("2 _ Mostrar Datos")
    print("3 _ Listado por familia")
    print("4 _ Mayor importe")
    print("5 _ Buscar factura")
    print("6 _ Salir")
    return int(input("Ingrese opción: "))


def cargar_venta():
    nro = int(input("Ingrese número de factura: "))
    nombre = input("Ingrese nombre del cliente: ")
    semana = int(input("Ingrese número de semana: "))
    familia = int(input("Ingrese familia de producto: "))
    monto = float(input("Ingrese monto de la venta: "))
    return Venta(nro, nombre, semana, familia, monto)


def cargar_venta_random():
    nro = random.randint(1, 10000)
    nombre = chr(random.randint(65, 90))
    semana = random.randint(1, 26)
    familia = random.randint(0, 9)
    monto = random.randint(100, 50000) / 100
    return Venta(nro, nombre, semana, familia, monto)


def cargar_datos(n):
    v = [None] * n
    for i in range(n):
        v[i] = cargar_venta_random()
    return v


def mostrar_ventas(v):
    for venta in v:
        write(venta)


def filtrar_familia(v, familia):
    filtrado = []
    for venta in v:
        if venta.familia == familia:
            filtrado.append(venta)
    return filtrado


def ordenar_semana(v):
    n = len(v)
    for i in range(n-1):
        for j in range(i + 1, n):
            if v[i].semana > v[j].semana:
                v[i], v[j] = v[j], v[i]


def listar_ventas_familia(v, familia):
    ventas_familia = filtrar_familia(v, familia)
    ordenar_semana(ventas_familia)
    mostrar_ventas(ventas_familia)


def buscar_mayor(v):
    mayor = v[0]
    for i in range(1, len(v)):
        if v[i].monto > mayor.monto:
            mayor = v[i]

    return mayor


def buscar_venta(v, nro):
    posicion = -1

    for i in range(len(v)):
        if v[i].nro == nro:
            posicion = i
            break

    return posicion


def principal():
    op = 0
    v = []
    while op != 6:
        op = menu()
        if op == 1:
            n = int(input("Ingrese la cantidad de ventas: "))
            v = cargar_datos(n)
        elif op == 2:
            mostrar_ventas(v)
        elif op == 3:
            familia = int(input("Ingrese familia a listar: "))
            listar_ventas_familia(v, familia)
        elif op == 4:
            mayor = buscar_mayor(v)
            print("La venta con mayor importe es: ")
            write(mayor)
        elif op == 5:
            nro = int(input("Ingrese factura a buscar: "))
            posicion = buscar_venta(v, nro)
            if posicion == -1:
                print("No se encontró la venta: ")
            else:
                write(v[posicion])



if __name__ == '__main__':
    principal()

