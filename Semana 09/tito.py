import random

x = random.randint(20, 100)
y = random.randint(20, 100)
posicion_inicial = (x, y)

op = 0
while op != 5:
    print("Posición: ", (x, y))
    print(" 1_ Girar al norte y moverse 10 pasos")
    print(" 2_ Girar al sur y moverse 20 pasos")
    print(" 3_ Girar al este y moverse 10 pasos")
    print(" 4_ Girar al oeste y moverse 20 pasos")
    print(" 5_ Salir")
    op = int(input("Ingrese opción: "))

    if op == 1:
        y -= 10
    elif op == 2:
        y += 20
    elif op == 3:
        x += 10
    elif op == 4:
        x -= 20
    elif op == 5:
        print("Chau!")
    else:
        print("Error. opción incorrecta")

