prenda1 = int(input("Ingrese tipo de prenda: "))
precio1 = float(input("Ingrese precio: "))
super_puntos1 = int(input("Participa super puntos: "))

prenda2 = int(input("Ingrese tipo de prenda: "))
precio2 = float(input("Ingrese precio: "))
super_puntos2 = int(input("Participa super puntos: "))

prenda3 = int(input("Ingrese tipo de prenda: "))
precio3 = float(input("Ingrese precio: "))
super_puntos3 = int(input("Participa super puntos: "))

total = precio1 + precio2 + precio3

if prenda1 == prenda2 and prenda2 == prenda3:
    ahorro = min(precio1, precio2, precio3)
elif prenda1 == prenda2:
    ahorro = max(precio1, precio2) / 2
elif prenda1 == prenda3:
    ahorro = max(precio1, precio3) / 2
elif prenda2 == prenda3:
    ahorro = max(precio2, precio3) / 2
else:
    ahorro = 0

total_promo = total - ahorro
forma_pago = int(input("Ingrese la forma de pago: "))
if forma_pago == 1:
    total_final = total_promo * 0.9
else:
    cuotas = int(input("Ingrese la cantidad de cuotas: "))
    if cuotas <= 3:
        total_final = total_promo * 1.02
    else:
        total_final = total_promo * 1.05
