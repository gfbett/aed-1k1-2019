import random

i = 0
while i < 100:
    n = random.randint(1, 100)
    print(n)
    if n == 42:
        print("Se encontró la respuesta a la última pregunta")
        break
    i += 1
else:
    print("No se encontró la respuesta")
