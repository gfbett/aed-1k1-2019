es_primer_par = es_primer_impar = True

n = int(input("Ingrese la cantidad de números a ingresar: "))
while n <= 0:
    n = int(input("Error. La cantidad debe ser positiva. Ingrese nuevamente: "))

for i in range(n):

    num = int(input("Ingrese un número: "))

    if num % 2 == 0:
        if es_primer_par:
            mayor = num
            es_primer_par = False
        elif num > mayor:
            mayor = num
    else:
        if es_primer_impar:
            menor = num
            es_primer_impar = False
        elif num < menor:
            menor = num

if es_primer_impar:
    print("No se ingresaron números impares")
else:
    print("El menor de los impares es:", menor)

if es_primer_par:
    print("No se ingresaron números pares")
else:
    print("El mayor de los pares es:", mayor)
