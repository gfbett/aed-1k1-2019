import invitados
import pickle
import os

ARCHIVO = "invitados.dat"
ONG = "Missing Children", "Aldeas", "Caritas", "Fundaleu", "PUPI", "Cimientos", "Médicos Sin Fronteras", "Uniendo Caminos", "Vida Silvestre", "Adoptare"


def menu():
    print("1 _ Listar invitados")
    print("2 _ Invitados por ONG y mesa")
    print("3 _ Invitado con mayor donación")
    print("4 _ Generar archivo donaciones por ONG")
    print("5 _ Buscar invitado")
    print("6 _ Salir")
    return int(input("Ingrese la opción: "))


def insertar_ordenado(v, invitado):
    n = len(v)
    izq, der = 0, n - 1
    pos = n
    while izq <= der:
        c = (izq + der) // 2
        if v[c].nombre == invitado.nombre:
            pos = c
            break
        elif invitado.nombre > v[c].nombre:
            izq = c + 1
        else:
            der = c - 1

    if izq > der:
        pos = izq

    v[pos:pos] = [invitado]


def cargar_invitados():
    v = []
    archivo = open(ARCHIVO, "rb")
    size = os.path.getsize(ARCHIVO)

    while archivo.tell() < size:
        invitado = pickle.load(archivo)
        insertar_ordenado(v, invitado)
    archivo.close()
    return v


def mostrar_invitados(v):
    for inv in v:
        print(invitados.write(inv))


def totalizar_mesa_ong(v):
    m = [[0] * 10 for i in range(13)]

    for invitado in v:
        mesa = invitado.mesa
        ong = invitado.ong

        m[mesa][ong] += 1

    return m


def mostrar_matriz(totales):
    for i in range(len(totales)):
        for j in range(len(totales[i])):
            if totales[i][j] > 0:
                print("Mesa:", i, "ONG:", ONG[j], "Invitados:", totales[i][j])


def generar_archivo(v, ong):
    archivo = open("donaciones" + str(ong) + ".dat", "wb")
    for invitado in v:
        if invitado.ong == ong:
            pickle.dump(invitado, archivo)
    archivo.close()


def total_mesa_ong(ong):
    nombre = "donaciones{}.dat".format(ong)
    if os.path.exists(nombre):
        archivo = open(nombre, "rb")
        size = os.path.getsize(nombre)
        while archivo.tell() < size:
            invitado = pickle.load(archivo)

        archivo.close()
    else:
        print("No se generó el archivo para la ong:", ong)


def principal():
    op = 1
    while op != 6:
        op = menu()
        if op == 1:
            v = cargar_invitados()
            mostrar_invitados(v)
        elif op == 2:
            totales = totalizar_mesa_ong(v)
            mostrar_matriz(totales)
        elif op == 3:
            pass
        elif op == 4:
            ong = int(input("Ingrese código de ong: "))
            generar_archivo(v, ong)
        elif op == 5:
            ong = int(input("Ingrese código de ong: "))
            total_mesa_ong(ong)


if __name__ == '__main__':
    principal()
