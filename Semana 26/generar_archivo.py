import invitados
import pickle


def generar_archivo(n):
    archivo = open("invitados.dat", "wb")
    for i in range(n):
        invitado = invitados.cargar_invitado_random()
        pickle.dump(invitado, archivo)
    archivo.close()


if __name__ == '__main__':
    n = int(input("Ingrese cantidad de invitados: "))
    generar_archivo(n)
